# Game Backup

Recommend convertor from EPS to SVG: https://image.online-convert.com/convert/eps-to-svg

## Assets Credit

### Items

- https://www.freepik.com/free-vector/colored-fruit-collection_991428.htm
- https://www.freepik.com/free-vector/set-delicious-fruits_1023065.htm
- https://www.freepik.com/free-vector/different-pieces-butchery_848830.htm

### Baskets

- https://www.freepik.com/free-vector/empty-baskets-set-wicker-boxes-hampers-containers-storage_11235372.htm
- https://www.freepik.com/free-vector/empty-basket-basket-full-apples_1172874.htm

### NPCs

- https://www.freepik.com/free-vector/smiling-animals-collection_1121430.htm

### Decorations

- https://www.freepik.com/free-vector/green-grass-pattern-set_9175193.htm
- https://www.freepik.com/free-vector/mountains-rocks-flat-icon-collection_8610212.htm

### Backgrounds

- https://www.template.net/editable/99415/nature-grass-background
- https://www.template.net/editable/110012/grass-and-mountain-background

