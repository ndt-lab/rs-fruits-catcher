use bevy::{prelude::*, sprite::collide_aabb::collide, window::WindowResolution};
use rand::prelude::*;

#[derive(Component)]
struct Player;
#[derive(Component)]
struct Name(String);
#[derive(Component)]
struct Fruit;
#[derive(Component)]
struct Meat;
#[derive(Component)]
struct Speed(f32);
#[derive(Resource)]
struct Score(u32);

fn main() {
    App::new()
        .insert_resource(ClearColor(Color::ALICE_BLUE))
        .insert_resource(Score(0))
        .add_plugins(DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                resolution: WindowResolution::new(900.00, 600.00),
                title: "Catching Game".to_string(),
                resizable: false,
                ..default()
            }),
            ..default()
        }))
        .add_systems(Startup, setup)
        .add_systems(Update, (move_player, drop_fruit, drop_meat))
        .add_systems(PostUpdate, (player_meets_fruit, player_meets_meat, score_display))
        .run();
}

fn setup(mut cmd: Commands, assets: Res<AssetServer>, score: Res<Score>) {
    let player_handle: Handle<Image> = assets.load("img/basket.png");
    let fruit_handle: Handle<Image> = assets.load("img/fruit.png");
    let meat_handle: Handle<Image> = assets.load("img/meat.png");
    let bg_handle: Handle<Image> = assets.load("img/bg.jpg");
    let font_handle: Handle<Font> = assets.load("font/Gluten-Regular.ttf");

    cmd.spawn(Camera2dBundle::default());

    cmd.spawn(TextBundle {
        text: Text {
            alignment: TextAlignment::Left,
            sections: vec![
                TextSection {
                    value: "Score: ".to_string(),
                    style: TextStyle {
                        // font: font_handle.clone_weak(),
                        font: font_handle.clone(),
                        font_size: 24.,
                        color: Color::Hsla {
                            hue: 23.,
                            saturation: 1.,
                            lightness: 0.5,
                            alpha: 1.,
                        },
                        // ..default()
                    },
                },
                TextSection {
                    value: "---".to_string(),
                    style: TextStyle {
                        // font: font_handle.clone_weak(),
                        font: font_handle.clone(),
                        font_size: 24.,
                        color: Color::Hsla {
                            hue: 23.,
                            saturation: 1.,
                            lightness: 0.5,
                            alpha: 1.,
                        },
                        // ..default()
                    },
                },
            ],
            ..default()
        },
        style: Style {
            position_type: PositionType::Absolute,
            top: Val::Px(10.0),
            left: Val::Px(8.0),
            ..default()
        },
        transform: Transform {
            translation: Vec3 { z: 9., ..default() },
            ..default()
        },
        ..default()
    });

    // Background
    cmd.spawn(SpriteBundle {
        texture: bg_handle,
        transform: Transform {
            translation: Vec3 {
                x: 0.,
                y: 0.,
                z: -9.,
            },
            ..default()
        },
        ..default()
    });

    cmd.spawn(Player)
        .insert(Name("basket".to_string()))
        .insert(SpriteBundle {
            texture: player_handle,
            transform: Transform {
                translation: Vec3 {
                    x: rand_f32(-450., 450.),
                    y: -300.,
                    z: 1.,
                },
                scale: Vec3 {
                    x: 0.3f32,
                    y: 0.32f32,
                    z: 0.,
                },
                // rotation: (),
                ..default()
            },
            ..default()
        });

    for i in 1..9 {
        cmd.spawn(Fruit)
            .insert(Name(format!("fruit {i}")))
            .insert(SpriteBundle {
                texture: fruit_handle.clone(),
                transform: Transform {
                    translation: Vec3 {
                        x: rand_f32(-450., 450.),
                        y: rand_f32(350., 450.),
                        z: 0.,
                    },
                    scale: Vec3 {
                        x: 0.15f32,
                        y: 0.15f32,
                        z: 0.,
                    },
                    ..default()
                },
                ..default()
            })
            .insert(Speed(rand_f32(150., 300.)));
    }

    for i in 1..4 {
        cmd.spawn(Meat)
            .insert(Name(format!("meat {i}")))
            .insert(SpriteBundle {
                texture: meat_handle.clone(),
                transform: Transform {
                    translation: Vec3 {
                        x: rand_f32(-450., 450.),
                        y: rand_f32(350., 450.),
                        z: 0.,
                    },
                    scale: Vec3 {
                        x: 0.15f32,
                        y: 0.15f32,
                        z: 0.,
                    },
                    ..default()
                },
                ..default()
            })
            .insert(Speed(rand_f32(150., 300.)));
    }
}

fn drop_item(mut transform: Mut<'_, Transform>, delta_sec: f32, speed: &Speed) {
    // distance(px) = speed(px/s) × time(s)
    transform.translation.y -= speed.0 * delta_sec;

    // re position the object
    if transform.translation.y < -350. {
        transform.translation.x = rand_f32(-450., 450.);
        transform.translation.y = rand_f32(330., 340.);
    }
}

fn drop_fruit(time: Res<Time>, mut query: Query<(&mut Transform, &Speed), With<Fruit>>) {
    for (transform, speed) in &mut query.iter_mut() {
        drop_item(transform, time.delta_seconds(), speed);
    }
}

fn drop_meat(time: Res<Time>, mut query: Query<(&mut Transform, &Speed), With<Meat>>) {
    for (transform, speed) in &mut query.iter_mut() {
        drop_item(transform, time.delta_seconds(), speed);
    }
}

fn move_player(
    time: Res<Time>,
    keyboard: Res<Input<KeyCode>>,
    mut query: Query<(&mut Transform, &Handle<Image>), With<Player>>,
    assets: Res<Assets<Image>>,
) {
    let (mut transform, img_handle) = query.single_mut();
    let bounding_img = get_bound_sz(&transform, img_handle, &assets);

    transform.translation.y = -300. + bounding_img.max.y;

    // distance(px) = speed(px/s) × time(s)
    if keyboard.pressed(KeyCode::Left) || keyboard.pressed(KeyCode::A) {
        let edge = -450. + bounding_img.max.x;
        if transform.translation.x <= edge {
            transform.translation.x = edge;
            return ();
        }
        transform.translation.x -= 500. * time.delta_seconds();
    }
    if keyboard.pressed(KeyCode::Right) || keyboard.pressed(KeyCode::D) {
        let edge = 450. - bounding_img.max.x;
        if transform.translation.x >= edge {
            transform.translation.x = edge;
            return ();
        }
        transform.translation.x += 500. * time.delta_seconds();
    }
}

fn player_meets_fruit(
    mut score: ResMut<Score>,
    mut params: ParamSet<(
        Query<(&Transform, &Handle<Image>), With<Player>>,
        Query<(&mut Transform, &Handle<Image>), With<Fruit>>,
    )>,
    assets: Res<Assets<Image>>,
) {
    // let mut fruit_sz: Rect;
    let mut player_sz = Rect {
        min: Vec2 { x: 0., y: 0. },
        max: Vec2 { x: 0., y: 0. },
    };
    let mut player_pos = Vec3 {
        x: 0.,
        y: 0.,
        z: 0.,
    };

    for (transform, img_handle) in params.p0().iter_mut() {
        player_sz = get_bound_sz(&transform, img_handle, &assets);
        player_pos = transform.translation;
        player_pos.z = 0.;
    }

    for (mut transform, img_handle) in params.p1().iter_mut() {
        let fruit_sz = get_bound_sz(&transform, img_handle, &assets);
        let mut fruit_pos = transform.translation;
        fruit_pos.z = 0.;

        if collide(
            player_pos,
            player_sz.max,
            transform.translation,
            fruit_sz.max,
        )
        .is_some()
        {
            transform.translation.x = rand_f32(-450., 450.);
            transform.translation.y = rand_f32(330., 340.);
            score.0 += 1;
        }
    }
}

fn player_meets_meat(
    mut score: ResMut<Score>,
    mut params: ParamSet<(
        Query<(&Transform, &Handle<Image>), With<Player>>,
        Query<(&mut Transform, &Handle<Image>), With<Meat>>,
    )>,
    assets: Res<Assets<Image>>,
) {
    // let mut fruit_sz: Rect;
    let mut player_sz = Rect {
        min: Vec2 { x: 0., y: 0. },
        max: Vec2 { x: 0., y: 0. },
    };
    let mut player_pos = Vec3 {
        x: 0.,
        y: 0.,
        z: 0.,
    };

    for (transform, img_handle) in params.p0().iter_mut() {
        player_sz = get_bound_sz(&transform, img_handle, &assets);
        player_pos = transform.translation;
        player_pos.z = 0.;
    }

    for (mut transform, img_handle) in params.p1().iter_mut() {
        let meat_sz = get_bound_sz(&transform, img_handle, &assets);
        let mut meat_pos = transform.translation;
        meat_pos.z = 0.;

        if collide(
            player_pos,
            player_sz.max,
            transform.translation,
            meat_sz.max,
        )
        .is_some()
        {
            transform.translation.x = rand_f32(-450., 450.);
            transform.translation.y = rand_f32(330., 340.);
            score.0 -= 1;
        }
    }
}

fn score_display(score: Res<Score>, mut query: Query<&mut Text>) {
    if !score.is_changed() {
        return ();
    }

    let mut text = query.single_mut();
    text.sections[1].value = score.0.to_string();
    println!(
        "{} {} {}",
        text.sections[0].value, text.sections[1].value, score.0
    );
}

fn get_bound_sz(
    transform: &Transform,
    img_handle: &Handle<Image>,
    assets: &Res<Assets<Image>>,
) -> Rect {
    let img_sz = match assets.get(img_handle) {
        None => Vec2 { x: 0., y: 0. },
        Some(img) => img.size(),
    };
    let scaled_img = img_sz * transform.scale.truncate();
    Rect::from_center_size(transform.scale.truncate(), scaled_img)
}

fn rand_f32(min: f32, max: f32) -> f32 {
    let mut rng = rand::thread_rng();
    rng.gen_range(min..max)
}
